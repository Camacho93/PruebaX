﻿using Integrador.common.Models;
using MVC_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC_Client.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogIn(UserLogin userLogin)
        {
            if (ModelState.IsValid)
            {

                if (string.IsNullOrEmpty(userLogin.Password))
                    ModelState.AddModelError("", "La contraseña no puede ser vacia.");
                else
                {
                    UserClient userClient = new UserClient();
                    if (userClient.Login(userLogin))
                    {
                        FormsAuthentication.SetAuthCookie(userLogin.LoginName, false);
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Informacion incorrecta.");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(userLogin);
        }

        [Authorize]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("LogIn", "Account");
        }
    }
}