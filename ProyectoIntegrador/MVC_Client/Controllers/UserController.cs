﻿using Integrador.common.Models;
using MVC_Client.Models;
using MVC_Client.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_Client.Controllers
{
    public class UserController : Controller
    {
        // GET: User

        [Authorize]
        public ActionResult Index()
        {
            UserClient userClient = new UserClient();  
            ViewBag.list= userClient.findAll();  

            if(ViewBag.list.Count <=0)
                return RedirectToAction("Create");
            return View(ViewBag.list);  
        }
        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }
        [HttpPost]
        public ActionResult Create(UserManager userManager)
        {
            UserClient userClient = new UserClient();
            userClient.Create(userManager);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            UserClient userClient = new UserClient();
            userClient.Delete(id);
            return RedirectToAction("Index");
        }
        [Authorize]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            UserClient userClient = new UserClient();
            UserManager userManager = new UserManager();
            userManager = userClient.find(id);
            return View("Edit", userManager);
        }
        [HttpPost]
        public ActionResult Edit(UserManager userManager)
        {
            UserClient userClient = new UserClient();
            userClient.Edit(userManager);
            return RedirectToAction("Index");
        }
    }
}