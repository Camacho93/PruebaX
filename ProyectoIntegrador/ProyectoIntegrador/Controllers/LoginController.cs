﻿using Integrador.common.Models;
using ProyectoIntegrador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProyectoIntegrador.Controllers
{
    public class LoginController : ApiController
    {

        [HttpPost]
        public IHttpActionResult LogIn(UserLogin UserLogin)
        {

            string password = GetUserPassword(UserLogin.LoginName);

            if (UserLogin.Password.Equals(password))
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }

        }


        public string GetUserPassword(string loginName)
        {
            using (ProyectoIntegradorEntities db = new ProyectoIntegradorEntities())
            {
                var user = db.User.Where(o => o.LoginName.Trim().Equals(loginName));
                if (user.Any())
                    return user.FirstOrDefault().PasswordEncryptedText;
                else
                    return string.Empty;
            }
        }
    }
}
