﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProyectoIntegrador.Models;
using Integrador.common.Models;

namespace ProyectoIntegrador.Controllers
{
    public class UserController : ApiController
    {
        private ProyectoIntegradorEntities db = new ProyectoIntegradorEntities();

        [HttpPost]
        public IHttpActionResult CreateUser(UserManager userManager)
        {

            using (ProyectoIntegradorEntities db = new ProyectoIntegradorEntities())
            {
                User user = new User();
                user.LoginName = userManager.LoginName;
                user.PasswordEncryptedText = userManager.Password;
                user.CreatedUserID = userManager.UserID > 0 ? userManager.UserID : 1;
                user.ModifiedUserID = userManager.UserID > 0 ? userManager.UserID : 1;
                user.CreatedDateTime = DateTime.Now;
                user.ModifiedDateTime = DateTime.Now;

                db.User.Add(user);
                db.SaveChanges();

                UserProfile userProfile = new UserProfile();
                userProfile.UserID = user.UserID;
                userProfile.FirstName = userManager.FirstName;
                userProfile.LastName = userManager.LastName;
                userProfile.Gender = userManager.Gender;
                userProfile.CreatedUserID = userManager.UserID > 0 ? userManager.UserID : 1;
                userProfile.ModifiedUserID = userManager.UserID > 0 ? userManager.UserID : 1;
                userProfile.CreatedDateTime = DateTime.Now;
                userProfile.ModifiedDateTime = DateTime.Now;

                db.UserProfile.Add(userProfile);
                db.SaveChanges();


                if (userManager.RoleID > 0)
                {
                    UserRole userRole = new UserRole();
                    userRole.RoleID = userManager.RoleID;
                    userRole.UserID = user.UserID;
                    userRole.IsActive = true;
                    userRole.CreatedUserID = userManager.UserID > 0 ? userManager.UserID : 1;
                    userRole.ModifiedUserID = userManager.UserID > 0 ? userManager.UserID : 1;
                    userRole.CreatedDateTime = DateTime.Now;
                    userRole.ModifiedDateTime = DateTime.Now;

                    db.UserRole.Add(userRole);
                    db.SaveChanges();
                }

                return StatusCode(HttpStatusCode.NoContent);
            }
        }

        public bool IsLoginNameExist(string loginName)
        {
            using (ProyectoIntegradorEntities db = new ProyectoIntegradorEntities())
            {
                return db.User.Where(o => o.LoginName.Equals(loginName)).Any();
            }
        }

        // GET: api/Users
        public IQueryable<UserManager> GetUser()
        {
            UserManager userManager = new UserManager();

            return (from u in db.User
                    join up in db.UserProfile on u.UserID equals up.UserID
                    join ur in db.UserRole on u.UserID equals ur.UserID
                    select new UserManager
                    {
                        UserID = u.UserID,
                        LoginName = u.LoginName,
                        Password = u.PasswordEncryptedText,
                        FirstName = up.FirstName,
                        LastName = up.LastName,
                        Gender = up.Gender,
                        RoleID = ur.RoleID
                    });
        }

        // GET: api/Users/5
        [ResponseType(typeof(UserManager))]
        public IHttpActionResult GetUser(int id)
        {

            UserManager user = (from u in db.User
                                join up in db.UserProfile on u.UserID equals up.UserID
                                join ur in db.UserRole on u.UserID equals ur.UserID

                                select new UserManager
                                {
                                    UserID = u.UserID,
                                    LoginName = u.LoginName,
                                    Password = u.PasswordEncryptedText,
                                    FirstName = up.FirstName,
                                    LastName = up.LastName,
                                    Gender = up.Gender,
                                    RoleID = ur.RoleID
                                }).Where(bases => bases.UserID == id).SingleOrDefault();

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPut]// PUT: api/Users/5

        public IHttpActionResult PutUser(int id, UserManager userManager)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userManager.UserID)
            {
                return BadRequest();
            }
            try
            {
                var user = db.User.Find(userManager.UserID);
                user.LoginName = userManager.LoginName;
                user.PasswordEncryptedText = userManager.Password;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();

                UserProfile userProfile = db.UserProfile.Where(x => x.UserID == userManager.UserID).SingleOrDefault();
                userProfile.FirstName = userManager.FirstName;
                userProfile.LastName = userManager.LastName;
                userProfile.Gender = userManager.Gender;
                db.Entry(userProfile).State = EntityState.Modified;
                db.SaveChanges();

                UserRole userRole = db.UserRole.Where(x => x.UserID == userManager.UserID).SingleOrDefault();
                userRole.RoleID = userManager.RoleID;
                db.Entry(userRole).State = EntityState.Modified;
                db.SaveChanges();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.User.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            UserProfile userProfile = db.UserProfile.Where(x => x.UserID == user.UserID).SingleOrDefault();
            db.UserProfile.Remove(userProfile);
            db.SaveChanges();
            UserRole userRole = db.UserRole.Where(x => x.UserID == user.UserID).SingleOrDefault();
            db.UserRole.Remove(userRole);
            db.SaveChanges();

            db.User.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.User.Count(e => e.UserID == id) > 0;
        }
    }
}